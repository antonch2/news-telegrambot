package main

import (
	"bot/internal/app/bot"
	"github.com/BurntSushi/toml"
	"log"
)



func main() {
	config := bot.NewConfig()

	_, err := toml.DecodeFile("configs/bot.example.toml", &config)
	if err != nil {
		log.Println(err)
	}
	newBotAPI, err := bot.NewBotAPI(config)
	if err != nil {
		log.Fatal(err)
	}
	if err := newBotAPI.Start(); err != nil {
		log.Fatal(err)
	}
}

