package store


type StoreConfig struct {
	Host string `yaml:"host"`
	Port string `yaml:"port"`
	Password string `yaml:"password"`
}

func NewStoreConfig() *StoreConfig {
	return &StoreConfig{}
}