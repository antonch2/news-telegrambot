package tests

import (
	"bot/internal/app/bot"
	"bot/internal/app/store"
	// "github.com/BurntSushi/toml"
	"github.com/stretchr/testify/assert"
	"testing"
)

type TestConfig struct {
	Token 			string `toml:"token"`
	LogFile			string `toml:"log_file"`
	LogLevel		string `toml:"log_level"`
	Store 			*store.StoreConfig
}


//func TestStore_ConnectDatabase(t *testing.T) {
//	conf := &TestConfig{}
//	_, err := toml.DecodeFile("../../../configs/bot.example.toml", &conf)
//	assert.NoError(t, err)
//	s := store.NewStore(&store.StoreConfig{
//		Host:     conf.Store.Host,
//		Port:     conf.Store.Port,
//		Password: conf.Store.Password,
//	})
//	_, err = s.ConnectDatabase()
//	assert.NoError(t, err)
//}


func TestBot_GetNews(t *testing.T) {

	smiles := make(map[string]string, 0)

	for smile, alias := range bot.HabrKb {
		smiles[smile] = alias
	}

	for smile, alias := range smiles {
		url := bot.Rss[bot.HabrKb[smile]]
		_, err := bot.GetNews(url)
		t.Run(alias, func (t *testing.T) {
			assert.Equal(t, nil, err)
		})
	}
}