package bot

import (
	"encoding/xml"
	"errors"
	"io/ioutil"
	"net/http"
)

type Item struct {
	URL string `xml:"link"`
	Title string `xml:"title"`
	PubDate string `xml:"pubDate"`
}

type RSS struct {
	Items []Item `xml:"channel>item"`
}

func GetNews(url string) (*RSS, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, errors.New("get_news http error")
	}
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)

	rss := new(RSS)
	err = xml.Unmarshal(body, &rss)
	if err != nil {
		return nil, errors.New("get_news cannot parse xml data")
	}
	return rss, nil
}