package bot

import (
	"fmt"
	tg "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
)

type User struct {
	ChatID 				int64
	Username 			string
	UpdateMessageID 	int
}


func (b *Bot) checkUpdates(updates tg.UpdatesChannel) {
	history := make([]int, 0)
	messages := make([]int, 0)
	var countUpdates int

	for update := range updates {

		if update.Message != nil {
			user := &User {
				ChatID:   update.Message.Chat.ID,
				Username: update.Message.Chat.UserName,
				UpdateMessageID: update.Message.MessageID,
			}
			switch update.Message.Command() {

			case "start":
				msg := tg.NewMessage(user.ChatID, startMsg)
				msg.ReplyMarkup = tg.NewRemoveKeyboard(true)
				msg.ReplyMarkup = GenerateMarkup(HabrKb, 4)
				msg.ParseMode = "HTML"
				m, err := b.bot.Send(msg)
				if err != nil {
					b.logger.Error(err)
				}
				b.store.AddUser(b.logger, user.ChatID, user.Username)
				b.store.ProcessCommand(b.logger, user.ChatID, "start")
				history = append(history, m.MessageID, user.UpdateMessageID)

			case "news":
				msg := tg.NewMessage(user.ChatID, startMsg)
				msg.ReplyMarkup = tg.NewRemoveKeyboard(true)
				msg.ReplyMarkup = GenerateMarkup(NewsKb, 6)
				msg.ParseMode = "HTML"
				m, err := b.bot.Send(msg)
				if err != nil {
					b.logger.Error(err)
				}
				history = append(history, m.MessageID, user.UpdateMessageID)

			case "close":
				msg := tg.NewMessage(user.ChatID, closeMsg)
				msg.ParseMode = "HTML"
				msg.ReplyMarkup = tg.NewRemoveKeyboard(true)
				closeMsg, err := b.bot.Send(msg)
				if err != nil {
					b.logger.Error(err)
				}

				history = append(history, closeMsg.MessageID, user.UpdateMessageID)
				b.store.ProcessCommand(b.logger, user.ChatID, "close")
				go b.deleteMessages(messages, user.ChatID)
				go b.deleteMessages(history, user.ChatID)


			default:
				trash := make([]int, 0)
				b.logger.WithFields(logrus.Fields{
					"ID": user.ChatID,
					"username": user.Username,
				}).Debug(update.Message.Text)
				trash = append(trash, user.UpdateMessageID)
				go b.deleteMessages(trash, user.ChatID)

			}

			var kb bool = false
			var url string
			keyboard := make(map[string]string, 0)
			var size int
			if u, ok := Rss[HabrKb[update.Message.Text]]; ok {
				kb = true
				url = u
				keyboard = HabrKb
				size = 4
			}
			if u, ok := Rss[NewsKb[update.Message.Text]]; ok {
				kb = true
				url = u
				keyboard = NewsKb
				size = 6
			}

			if kb {
				messages = append(messages, user.UpdateMessageID)
				if countUpdates >= 1 {
					go b.deleteMessages(messages, user.ChatID)
				}
				countUpdates++
				rssResponse, err := GetNews(url)
				if err != nil {
					b.logger.Error(err)
				} else {
					for _, item := range rssResponse.Items[:3] {
						text := fmt.Sprintf(`<a href="%s">%s</a>`, item.URL, item.Title) + "\n" + fmt.Sprintf(`%s`, item.PubDate)
						msg := tg.NewMessage(user.ChatID, text)
						msg.ParseMode = "HTML"
						message, _ := b.bot.Send(msg)
						mid := message.MessageID
						messages = append(messages, mid)
					}
					msg := tg.NewMessage(user.ChatID, startMsg)
					msg.ReplyMarkup = GenerateMarkup(keyboard, size)
					msg.ParseMode = "HTML"
					closeMsg, _ := b.bot.Send(msg)
					messages = append(messages, closeMsg.MessageID, user.UpdateMessageID)
				}
			}

			switch update.Message.Text {
			case closeMsg:
				msg := tg.NewMessage(user.ChatID, deleteMsg)
				msg.ParseMode = "HTML"
				closeMsg, _ := b.bot.Send(msg)
				history = append(history, closeMsg.MessageID, user.UpdateMessageID)
				go b.deleteMessages(messages, user.ChatID)
				go b.deleteMessages(history, user.ChatID)
			}
		}

		if update.CallbackQuery != nil {

			// catch callback...
		}
	}
}


func (b *Bot) deleteMessages(messages []int, ChatID int64) {
	for i := 0; i < len(messages); i++ {
		d := tg.DeleteMessageConfig{
			ChatID:          ChatID,
			MessageID:       messages[i],
		}
		_, err := b.bot.DeleteMessage(d)
		if err != nil {
			continue
		}
	}
}
