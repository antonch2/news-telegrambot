package bot

import (
	"bot/internal/app/store"
)

type Config struct {
	Token 			string `toml:"token"`
	LogFile			string `toml:"log_file"`
	LogLevel		string `toml:"log_level"`
	Store 			*store.StoreConfig
}

func NewConfig() *Config {
	return &Config {
		Token: "",
		LogFile: "",
		LogLevel: "debug",
		Store: store.NewStoreConfig(),
	}
}