package bot

import (
	tg "github.com/go-telegram-bot-api/telegram-bot-api"
)


var HabrKb map[string]string = map[string]string {
	"🔝 хабр": "habr best",
	"🖥 хабр": "habr adm",
	"💼 хабр": "habr career",
	"💰 хабр": "habr fin",
	"🐍 хабр": "habr python",
	"🔮 хабр": "habr science",
	"🛡 хабр": "habr law",
	"🌏 хабр": "habr networks",
	"🏢 хабр": "habr it-comp",
	"🛠 хабр": "habr develop",
	"〽️ хабр": "habr go",
	"📨 хабр": "habr news",
	"🔩 хабр": "habr devops",
	"⚡️ хабр": "habr go",
	"☄️ хабр": "habr space",
	"👾 хабр": "habr games",
	"✍️ хабр": "habr algo",
	"🧠 хабр": "habr brain",
	"🤖 хабр": "habr ii",
	"💡 хабр": "habr hacks",
}

var NewsKb map[string]string = map[string]string {
	"©️": "cnews",
	"◾": "mediazona",
	"🌧": "tvrain",
	"⚙️": "popmech",
	"🔪": "knife",
	"📰" : "vedomosti",
	"🗞": "lenta",
	"🔸": "N+1",
	"📪": "iz",
	"🔭": "science",
	"🐙": "meduza",
	"🧓": "echo",
	"📒": "TJ",
	"📲": "hi-news",
	"🎮": "igromania",
	"📣": "tass",
	"🔹": "vesti",
	"🔺": "aif",
	"♻": "russia_today",
	"🔊": "rbk",
	"📧": "rgru",
	"⚽": "sports",
	"🏆": "championat",
	"💠": "ria",
	"🌀": "riamo",
	"💡": "rtech",
	"🗣": "rpolitics",
	"🌐": "rworld",
	"📠": "fax",
	"Ⓜ": "mash",
}

func GenerateMarkup(data map[string]string, size int) tg.ReplyKeyboardMarkup {
	m := tg.ReplyKeyboardMarkup{}
	var row []tg.KeyboardButton

	for article, _ := range data {
		btn := tg.NewKeyboardButton(article)
		row = append(row, btn)
		if len(row) == size {
			m.Keyboard = append(m.Keyboard, row)
			row = nil
		}
	}
	if row != nil {
		m.Keyboard = append(m.Keyboard, row)
	}
	row = nil
	btn := tg.NewKeyboardButton(closeMsg)
	row = append(row, btn)
	m.Keyboard = append(m.Keyboard, row)

	return m
}