package bot

import (
	"bot/internal/app/store"
	tg "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"os"
	"time"
)

type Bot struct {
	config *Config
	store *store.Store
	bot *tg.BotAPI
	logger *logrus.Logger
}

func NewBotAPI(conf *Config) (*Bot, error) {
	botAPI, err := tg.NewBotAPI(conf.Token)
	if err != nil {
		return nil, err
	}
	return &Bot{
		bot: botAPI,
		config: conf,
		logger: logrus.New(), // new instance of the logger
	}, nil
}

func (b *Bot) Start() error {

	if err := b.configureLogger(); err != nil {
		return err
	}

	if err := b.configureStore(); err != nil {
		b.logger.Println(err)
	}
	b.logger.Info("Connected to Redis Database!")

	botUpdates, err := b.configureBot()
	if err != nil {
		return err
	}

	b.checkUpdates(botUpdates)

	return nil
}

func (b *Bot) configureBot() (tg.UpdatesChannel, error) {
	bot, err := tg.NewBotAPI(b.config.Token)
	if err != nil {
		return nil, err
	}

	bot.Debug = false
	b.logger.Infof("Authorized on account %s", bot.Self.UserName)
	u := tg.NewUpdate(0)
	u.Timeout = 30
	// канал, куда прилетают обновления от API
	updates, _ := bot.GetUpdatesChan(u)
	time.Sleep(500 * time.Millisecond)
	updates.Clear()
	return updates, nil
}

func (b *Bot) configureStore() error {
	st := store.NewStore(b.config.Store)
	b.store = st

	rdb, err := st.ConnectDatabase()
	if err != nil {
		return err
	}
	b.store.Rdb = rdb
	return nil
}

func (b *Bot) configureLogger() error {
	level, err := logrus.ParseLevel(b.config.LogLevel)
	if err != nil {
		return err
	}
	b.logger.SetLevel(level)
	b.logger.SetFormatter(&logrus.JSONFormatter{
		TimestampFormat:   "2006-01-02 15:04:05",
	})
	logFile, err := os.OpenFile(b.config.LogFile, os.O_APPEND | os.O_CREATE | os.O_WRONLY, 0666)
	if err != nil {
		b.logger.Warn("Log Out is set to console")
	}
	b.logger.Out = logFile
	return nil
}