.PHONY: build

compile:
	go get -v ./...

build:
	go build -v ./cmd/bot

test:
	go test -v ./internal/app/tests/...

.DEFAULT_GOAL := build